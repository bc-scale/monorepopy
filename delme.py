from enum import Enum


class Area(int, Enum):
    CITY = 1
    VILLAGE = 2


assert Area.CITY == 1
