# Прототип на Python мессенджера Wave

## Сервисы
| Название сервиса | Описание                                                                 |
| ---------------- |--------------------------------------------------------------------------|
| Producer | Запись сообщений в Kafka                                                 |
| Consumer | Запись сообщений в MongoDB                                               |
| MessagesFlow | REST API сервис чтение сообщений из MongoDB                              |
| Accounts | REST API сервис для работы с акаунтами пользователей (хранит в Postgres) |

## Переменные окружения
### Kafka
KAFKA_HOST=kafka \
KAFKA_PORT=29092
### MongoDB
MONGO_INITDB_ROOT_USERNAME: root \
MONGO_INITDB_ROOT_PASSWORD: example
### Postgres
POSTGRES_HOST=postgres-db \
POSTGRES_PORT=5432

## Локальное развертывания проекта
### docker-compose
> docker-compose -f ./docker-compose.dev.yml up -d
### k8s
> minikube start \
> skaffold run \
> minikube service list

## CI/CD развертывание
### gitlab-runner
