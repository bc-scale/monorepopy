CREATE ROLE postgres_admin;
ALTER ROLE postgres_admin WITH SUPERUSER INHERIT CREATEROLE CREATEDB LOGIN REPLICATION BYPASSRLS PASSWORD 'md5b59db012cd7590313bdee1d05ab3ab41';

SELECT 'CREATE DATABASE accounts WITH TEMPLATE = template0 ENCODING = ''UTF8'' LC_COLLATE = ''en_US.utf8'' LC_CTYPE = ''en_US.utf8'';'
WHERE NOT EXISTS (SELECT FROM pg_database WHERE datname = 'accounts')\gexec

ALTER DATABASE accounts OWNER TO postgres_admin;

SELECT 'CREATE DATABASE auth WITH TEMPLATE = template0 ENCODING = ''UTF8'' LC_COLLATE = ''en_US.utf8'' LC_CTYPE = ''en_US.utf8'';'
WHERE NOT EXISTS (SELECT FROM pg_database WHERE datname = 'auth')\gexec

ALTER DATABASE auth OWNER TO postgres_admin;
