from pydantic import BaseModel


class TranslateEnRu(BaseModel):
    message: str
    target_lang: str
