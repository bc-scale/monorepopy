import sys
from os.path import dirname
sys.path.append(dirname(dirname(dirname(__file__))))

from google_trans_new.google_trans_new import google_translator, google_new_transError

from fastapi.testclient import TestClient

from main import app
from translator import translatorAPI

client = TestClient(app)


def test_read_main():
    response = client.get('/')
    assert response.status_code == 200
    assert response.json() == {"Name service": "Translation"}


def test_translate_message():
    response = client.post('/', json={'message': 'hello world!', 'target_lang': 'ru'})

    assert response.status_code == 200
    assert response.text == '"Привет, мир! "', response.text


def test_get_translate_message_redis():
    response = client.post('/', json={'message': 'Green!', 'target_lang': 'ru'})

    assert response.status_code == 200
    assert response.text == '"Зеленый! "', response.text

    response = client.post('/', json={'message': 'Green!', 'target_lang': 'ru'})

    assert response.status_code == 200
    assert response.text == '"Зеленый! "', response.text


def test_google_trans():

    translatorAPI('1', '1')
