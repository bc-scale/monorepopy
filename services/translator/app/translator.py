from google_trans_new.google_trans_new import google_translator


def translatorAPI(message: str, target_lang: str):
    translator = google_translator()

    try:
        translate_text = translator.translate(message, lang_src='', lang_tgt=target_lang)
        return {'200': translate_text}
    except Exception as e:
        return {'Error': e}
