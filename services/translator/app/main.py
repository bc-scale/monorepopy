import sys
from os.path import dirname
sys.path.append(dirname(dirname(dirname(__file__))))

import aioredis
import uvicorn

from fastapi import FastAPI, status
from fastapi.responses import JSONResponse

from models import TranslateEnRu
from translator import translatorAPI

app = FastAPI()


async def redis_connect(message: str, target_lang: str):
    redis = aioredis.from_url('redis://redis', decode_responses=True)
    message_in_redis = target_lang + '$%^:' + message
    cache = await redis.get(message_in_redis)

    if cache:
        return cache
    else:
        result = translatorAPI(message, target_lang)
        if '200' in result.keys():
            await redis.set(message_in_redis, result['200'], ex=86400)
        return result

@app.get('/')
async def root():
    return {"Name service": "Translation"}


@app.post('/')
async def translate(*, body: TranslateEnRu):
    result = await redis_connect(body.message, body.target_lang)

    if isinstance(result, str):
        translate_text = result
    elif '200' in result.keys():
        translate_text = result['200']
    elif '400' in result.keys():
        translate_text = result['400']
        return JSONResponse(content=translate_text, status_code=status.HTTP_400_BAD_REQUEST,
                            media_type='application/json')
    else:
        return JSONResponse(content=result, status_code=status.HTTP_400_BAD_REQUEST,
                            media_type='application/json')

    return JSONResponse(content=translate_text, status_code=status.HTTP_200_OK, media_type='application/json')

if __name__ == '__main__':
    uvicorn.run(app, host="0.0.0.0", port=8001)
