from os import getenv
from functools import wraps
from logging import config, getLogger, DEBUG, ERROR
from logstash_async.handler import AsynchronousLogstashHandler
from logstash_async.formatter import LogstashFormatter

# Logger ELK
NAME_SERVICE = 'consumer'
LOGGER_ELK_HOST = getenv('LOGGER_ELK_HOST', 'elk')
LOGGER_ELK_PORT = getenv('LOGGER_ELK_PORT', 50000)
DATABASE_PATH = getenv('DATABASE_PATH', None)
LOGGING_LEVEL = getenv('LOGGING_LEVEL', 'DEBUG')


LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,

    'formatters': {
        'default_formatter': {
            'format': '[%(levelname)s:%(asctime)s] %(message)s'
        },
        'verbose': {
            'format': '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
        },
        'default': {
            '()': 'uvicorn.logging.DefaultFormatter',
            'fmt': '%(levelprefix)s %(message)s',
            'use_colors': None,
        },
        'access': {
            '()': 'uvicorn.logging.AccessFormatter',
            'fmt': "%(levelprefix)s %(client_addr)s - '%(request_line)s' %(status_code)s",
        },
    },

    'handlers': {
        'file': {
            'class': 'logging.FileHandler',
            'formatter': 'default_formatter',
            'filename': 'config/sender_log.log',
        },
        'console': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'formatter': 'verbose',
        },
        'default': {
            'formatter': 'default',
            'class': 'logging.StreamHandler',
            'stream': 'ext://sys.stdout',
        },
        'access': {
            'formatter': 'access',
            'class': 'logging.StreamHandler',
            'stream': 'ext://sys.stdout',
        },
    },

    'loggers': {
        'logger_file': {
            'handlers': ['file'],
            'level': 'INFO',
            'propagate': True
        },
        '': {
            'handlers': ['console', ],
            'level': 'INFO',
        },
        'uvicorn.error': {
            'level': 'INFO',
        },
        'uvicorn.access': {
            'handlers': ['access'],
            'level': 'INFO',
            'propagate': False,
        },
    },

    'logger_console': {
        'level': 'INFO',
        'formatter': 'verbose',
        'handlers': ['console', ],
    },
}

# config.dictConfig(LOGGING)
#
# logger_file = getLogger('logger_file')
# logger_console = getLogger(__name__)


logger_elk = getLogger('python-logstash-logger')
logger_elk.setLevel(LOGGING_LEVEL)

async_handler = AsynchronousLogstashHandler(LOGGER_ELK_HOST, LOGGER_ELK_PORT, database_path=DATABASE_PATH)

logstash_formatter = LogstashFormatter(
    message_type='python-logstash',
    extra=dict(service=NAME_SERVICE)
)

async_handler.setFormatter(logstash_formatter)

logger_elk.addHandler(async_handler)


def log_error(func):
    @wraps(func)
    def wrapper(*args, **kwargs):

        try:
            return func(*args, **kwargs)
        except Exception as e:
            error_msg = f'Error has occurred at func="{func.__name__}" with args={args} and kwargs={kwargs}\n\n'
            logger_elk.exception(error_msg)
            return e

    return wrapper



def log_func():
    def func_log(func):

        @wraps(func)
        def wrapper(*args, **kwargs):
            msg = f'func="{func.__name__}" with args={args} and kwargs={kwargs}\n\n'
            logger_elk.debug(msg)
            return func(*args, **kwargs)

        return wrapper

    return func_log

# def _generate_log_error(path):
#     """
#     Create a logger object
#     :param path: Path of the log file.
#     :return: Logger object.
#     """
#     # Create a logger and set the level.
#     logger = getLogger('LogError')
#     logger.setLevel(ERROR)
#
#     # Create file handler, log format and add the format to file handler
#     file_handler = logging.FileHandler(path)
#
#     # See https://docs.python.org/3/library/logging.html#logrecord-attributes
#     # for log format attributes.
#     log_format = '%(levelname)s %(asctime)s %(message)s'
#     formatter = logging.Formatter(log_format)
#     file_handler.setFormatter(formatter)
#
#     logger.addHandler(file_handler)
#     return logger
#
#
# def _generate_log_func(path):
#     # Create a logger and set the level.
#     logger = logging.getLogger('LogError')
#     logger.setLevel(logging.DEBUG)
#
#     # Create file handler, log format and add the format to file handler
#     file_handler = logging.FileHandler(path)
#
#     # See https://docs.python.org/3/library/logging.html#logrecord-attributes
#     # for log format attributes.
#     log_format = '%(levelname)s %(asctime)s %(message)s'
#     formatter = logging.Formatter(log_format)
#     file_handler.setFormatter(formatter)
#
#     logger.addHandler(file_handler)
#     return logger
