from prometheus_async.aio import time
from prometheus_client import Counter, Histogram

METRIC_REQUEST_CALL_TIMES = Counter('requests_call_total', 'HTTP requests', ['task_or_results', 'type_operation'])

# Messages
METRIC_CONSUMER_MESSAGES = Counter('requests_consumer_messages_total', 'Count consumer messages', ['role', 'type'])

# Time requests
# REQUEST_TIME = Summary('request_consumer_processing_seconds', 'Time spent processing request for consumer', )
REQ_TIME = Histogram("req_time_seconds", "time spent in requests")
TIME = time(REQ_TIME)

# create_account_timer = REQUEST_TIME.labels(method='post', endpoint='/accounts')
