# Kafka
import os

# Kafka
KAFKA_HOST = os.getenv('KAFKA_HOST', 'localhost')
KAFKA_PORT = os.getenv('KAFKA_PORT', 29092)

# MongoDB
MONGODB_HOST = os.getenv('FLOW_MONGODB_HOST', 'localhost')
MONGODB_PORT = os.getenv('FLOW_MONGODB_PORT', 27017)
MONGODB_USER = os.getenv('FLOW_MONGODB_USER', 'root')
MONGODB_PASSWORD = os.getenv('FLOW_MONGODB_PASSWORD', 'example')
