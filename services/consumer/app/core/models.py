import uuid
from enum import Enum
from pydantic import BaseModel


class Message(BaseModel):
    uid: str
    sender_uid: str
    text: str


class DirectMessage(Message):
    session_general_uid: str | None = None
    recipient_uid: str


class GroupMessage(Message):
    group_uid: str


class ChannelMessage(Message):
    channel_uid: str


class EditMessage(BaseModel):
    message_uid: str


class EditDirectMessage(EditMessage):
    text: str

class EditGroupMessage(EditMessage):
    text: str


class EditChannelMessage(EditMessage):
    text: str


class DeliveryStatus(str, Enum):
    SENT = 'sent'
    DELIVERED = 'delivered'
    READ = 'read'


class DeliveryStatusMessage(BaseModel):
    message_uid: str


class DirectDeliveryStatusMessage(DeliveryStatusMessage):
    status: DeliveryStatus


class GroupDeliveryStatusMessage(DeliveryStatusMessage):
    status: DeliveryStatus


class ChannelDeliveryStatusMessage(DeliveryStatusMessage):
    status: DeliveryStatus
