from kafka import KafkaConsumer
from enum import Enum
from .config import KAFKA_HOST, KAFKA_PORT


class KafkaTopic(str, Enum):
    DIRECTS = "directs"
    DIRECTS_EDIT = "directs_edit"
    CHANNELS = "channels"
    CHANNELS_EDIT = "channels_edit"
    GROUPS = "groups"
    GROUPS_EDIT = "groups_edit"
    DIRECT_MESSAGE_STATUS = "direct_message_status"
    CHANNEL_MESSAGE_STATUS = "channel_message_status"
    GROUP_MESSAGE_STATUS = "group_message_status"
    DIRECT_MESSAGE_STATUS_UPDATE = "direct_message_status_update"
    CHANNEL_MESSAGE_STATUS_UPDATE = "channel_message_status_update"
    GROUP_MESSAGE_STATUS_UPDATE = "group_message_status_update"


# def get_kafka() -> KafkaConsumer:
#     kafka = KafkaConsumer(auto_offset_reset='earliest', bootstrap_servers=f'{KAFKA_HOST}:{KAFKA_PORT}')  # auto_offset_reset='earliest',
#     try:
#         yield kafka
#     finally:
#         kafka.close()
#     return
