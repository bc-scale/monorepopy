from .interfaces import MessageStorage
from .models import Message, DeliveryStatusMessage, EditMessage
from pymongo import MongoClient
from .config import MONGODB_HOST, MONGODB_PORT, MONGODB_USER, MONGODB_PASSWORD
from .logger_elk import logger_elk
from .monitoring_metrics import METRIC_CONSUMER_MESSAGES


class MongoStorage(MessageStorage):

    def __init__(self):
        self.client = MongoClient(f'mongodb://{MONGODB_USER}:{MONGODB_PASSWORD}@{MONGODB_HOST}:{MONGODB_PORT}')

    def save_message(self, message: Message) -> None:
        match message.__class__.__name__:
            case 'DirectMessage':
                db = self.client.messages.direct_messages
                db.insert_one(message.dict())
            case 'GroupMessage':
                db = self.client.messages.group_messages
                db.insert_one(message.dict())
            case 'ChannelMessage':
                db = self.client.messages.channel_messages
                db.insert_one(message.dict())
            case _:
                logger_elk.error(f' Save Unknown Message Type {message.__class__.__name__}')
                METRIC_CONSUMER_MESSAGES.labels(role='consumer', type='UNKNOWN_MESSAGE_TYPE').inc()
                raise ValueError("Unknown Message Type")

    def edit_message(self, message: EditMessage) -> None:
        match message.__class__.__name__:
            case 'EditDirectMessage':
                db = self.client.messages.direct_messages
                db.update_one({'uid': message.message_uid}, {"$set": {"text": message.text}})
            case 'EditGroupMessage':
                db = self.client.messages.group_messages
                db.update_one({'uid': message.message_uid}, {"$set": {"text": message.text}})
            case 'EditChannelMessage':
                db = self.client.messages.channel_messages
                db.update_one({'uid': message.message_uid}, {"$set": {"text": message.text}})
            case _:
                logger_elk.error(f' Edit Unknown Message Type {message.__class__.__name__}')
                METRIC_CONSUMER_MESSAGES.labels(role='consumer', type='UNKNOWN_MESSAGE_TYPE').inc()
                raise ValueError("Unknown Message Type")



    def save_delivery_status(self, delivery_status: DeliveryStatusMessage) -> None:
        match delivery_status.__class__.__name__:
            case 'DirectDeliveryStatusMessage':
                 db = self.client.delivery_statuses.directs
                 db.insert_one(delivery_status.dict())
            case 'GroupDeliveryStatusMessage':
                db = self.client.delivery_statuses.groups
                db.insert_one(delivery_status.dict())
            case 'ChannelDeliveryStatusMessage':
                db = self.client.delivery_statuses.channels
                db.insert_one(delivery_status.dict())
            case _:
                logger_elk.error(f' Edit Unknown Delivery Type {delivery_status.__class__.__name__}')
                raise ValueError("Unknown Message Type")


    def update_delivery_status(self, delivery_status: DeliveryStatusMessage) -> None:
        match delivery_status.__class__.__name__:
            case 'DirectDeliveryStatusMessage':
                 db = self.client.delivery_statuses.directs
                 db.update_one({'message_uid': delivery_status.message_uid}, {"$set": {"status": delivery_status.status}})
            case 'GroupDeliveryStatusMessage':
                db = self.client.delivery_statuses.groups
                db.update_one({'message_uid': delivery_status.message_uid}, {"$set": {"status": delivery_status.status}})
            case 'ChannelDeliveryStatusMessage':
                db = self.client.delivery_statuses.channels
                db.update_one({'message_uid': delivery_status.message_uid}, {"$set": {"status": delivery_status.status}})
            case _:
                logger_elk.error(f' Update Unknown Delivery Type {delivery_status.__class__.__name__}')
                raise ValueError("Unknown Message Type")


    def clear_collection(self, collection_name: str) -> int:
        if collection_name in self.client.messages.list_collections():
            pass
