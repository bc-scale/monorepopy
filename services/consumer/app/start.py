import sys
from os.path import dirname
from multiprocessing import Pool
import json

from prometheus_client import start_http_server

sys.path.append(dirname(dirname(dirname(dirname(dirname(__file__))))))
sys.path.append(dirname(dirname(__file__)))

#print(sys.path)

from kafka import TopicPartition, KafkaConsumer

from core.dependencies import KafkaTopic
from core.config import KAFKA_HOST, KAFKA_PORT
from core.storages import MongoStorage
from core.logger_elk import logger_elk
from core.monitoring_metrics import METRIC_CONSUMER_MESSAGES
from core.models import DirectMessage,\
    GroupMessage,\
    ChannelMessage,\
    EditDirectMessage,\
    EditGroupMessage,\
    EditChannelMessage,\
    DirectDeliveryStatusMessage,\
    GroupDeliveryStatusMessage,\
    ChannelDeliveryStatusMessage


def listen_topic(topic: KafkaTopic) -> None:
    consumer = KafkaConsumer(auto_offset_reset='earliest',
                             bootstrap_servers=f'{KAFKA_HOST}:{KAFKA_PORT}')  # auto_offset_reset='earliest', latest
    try:
        mongo_storage = MongoStorage()
        logger_elk.debug(f'Fetching message from {topic.value} ...')

        consumer.assign([TopicPartition(topic.value, 0)])

        for msg in consumer:
            msg = json.loads(msg.value.decode())
            match topic:
                case KafkaTopic.DIRECTS:
                    new_message = DirectMessage(**msg)
                    mongo_storage.save_message(new_message)
                    METRIC_CONSUMER_MESSAGES.labels(role='consumer', type='DIRECTS').inc()
                case KafkaTopic.GROUPS:
                    new_message = GroupMessage(**msg)
                    mongo_storage.save_message(new_message)
                    METRIC_CONSUMER_MESSAGES.labels(role='consumer', type='GROUPS').inc()
                case KafkaTopic.CHANNELS:
                    new_message = ChannelMessage(**msg)
                    mongo_storage.save_message(new_message)
                    METRIC_CONSUMER_MESSAGES.labels(role='consumer', type='CHANNELS').inc()
                case KafkaTopic.DIRECTS_EDIT:
                    new_message = EditDirectMessage(**msg)
                    mongo_storage.edit_message(new_message)
                    METRIC_CONSUMER_MESSAGES.labels(role='consumer', type='DIRECTS_EDIT').inc()
                case KafkaTopic.CHANNELS_EDIT:
                    new_message = EditChannelMessage(**msg)
                    mongo_storage.edit_message(new_message)
                    METRIC_CONSUMER_MESSAGES.labels(role='consumer', type='CHANNELS_EDIT').inc()
                case KafkaTopic.GROUPS_EDIT:
                    new_message = EditGroupMessage(**msg)
                    mongo_storage.edit_message(new_message)
                    METRIC_CONSUMER_MESSAGES.labels(role='consumer', type='GROUPS_EDIT').inc()
                case KafkaTopic.DIRECT_MESSAGE_STATUS:
                    new_message = DirectDeliveryStatusMessage(**msg)
                    mongo_storage.save_delivery_status(new_message)
                    METRIC_CONSUMER_MESSAGES.labels(role='consumer', type='DIRECT_MESSAGE_STATUS').inc()
                case KafkaTopic.CHANNEL_MESSAGE_STATUS:
                    new_message = ChannelDeliveryStatusMessage(**msg)
                    mongo_storage.save_delivery_status(new_message)
                    METRIC_CONSUMER_MESSAGES.labels(role='consumer', type='CHANNEL_MESSAGE_STATUS').inc()
                case KafkaTopic.GROUP_MESSAGE_STATUS:
                    new_message = GroupDeliveryStatusMessage(**msg)
                    mongo_storage.save_delivery_status(new_message)
                    METRIC_CONSUMER_MESSAGES.labels(role='consumer', type='GROUP_MESSAGE_STATUS').inc()
                case KafkaTopic.DIRECT_MESSAGE_STATUS_UPDATE:
                    new_message = DirectDeliveryStatusMessage(**msg)
                    mongo_storage.update_delivery_status(new_message)
                    METRIC_CONSUMER_MESSAGES.labels(role='consumer', type='DIRECT_MESSAGE_STATUS_UPDATE').inc()
                case KafkaTopic.CHANNEL_MESSAGE_STATUS_UPDATE:
                    new_message = ChannelDeliveryStatusMessage(**msg)
                    mongo_storage.update_delivery_status(new_message)
                    METRIC_CONSUMER_MESSAGES.labels(role='consumer', type='CHANNEL_MESSAGE_STATUS_UPDATE').inc()
                case KafkaTopic.GROUP_MESSAGE_STATUS_UPDATE:
                    new_message = GroupDeliveryStatusMessage(**msg)
                    mongo_storage.update_delivery_status(new_message)
                    METRIC_CONSUMER_MESSAGES.labels(role='consumer', type='GROUP_MESSAGE_STATUS_UPDATE').inc()
                case _:
                    METRIC_CONSUMER_MESSAGES.labels(role='consumer', type='UNKNOWN_TOPIC').inc()
                    logger_elk.debug(f'Unknown KafkaTopic')
                    raise ValueError("Unknown KafkaTopic")

    except Exception as er:
        logger_elk.exception(f'[Error] {er}, topic: {topic.value}')
    consumer.close()
    logger_elk.info(f'{topic.value} finished')


if __name__ == "__main__":
    logger_elk.info('Worker-consumer is working ...')
    topics = [
        KafkaTopic.DIRECTS,
        KafkaTopic.GROUPS,
        KafkaTopic.CHANNELS,
        KafkaTopic.DIRECTS_EDIT,
        KafkaTopic.GROUPS_EDIT,
        KafkaTopic.CHANNELS_EDIT,
        KafkaTopic.DIRECT_MESSAGE_STATUS,
        KafkaTopic.CHANNEL_MESSAGE_STATUS,
        KafkaTopic.GROUP_MESSAGE_STATUS,
        KafkaTopic.DIRECT_MESSAGE_STATUS_UPDATE,
        KafkaTopic.CHANNEL_MESSAGE_STATUS_UPDATE,
        KafkaTopic.GROUP_MESSAGE_STATUS_UPDATE
    ]

    with Pool(len(topics)) as p:
        print(p.map(listen_topic, topics))
