import sys
from os.path import dirname
sys.path.append(dirname(dirname(dirname(__file__))))

from tests.databases import get_connection


def test_write_message():
    db = get_connection()
    text = 'new message 3'
    messages_out = db.find_one({"text": text})
    assert messages_out['text'] == text
