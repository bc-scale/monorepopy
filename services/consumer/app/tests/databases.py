from pymongo import MongoClient

from core.config import MONGODB_HOST, MONGODB_PORT, MONGODB_USER, MONGODB_PASSWORD


def get_connection():
    client = MongoClient(f'mongodb://{MONGODB_USER}:{MONGODB_PASSWORD}@{MONGODB_HOST}:{MONGODB_PORT}')
    try:
        return client.messages.direct_messages
    except Exception as er:
        raise er
