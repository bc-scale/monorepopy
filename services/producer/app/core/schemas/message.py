import uuid
from typing import Optional
from enum import Enum
from pydantic import BaseModel, Field


class MessageIn(BaseModel):
    sender_uid: str
    text: str


class MessageOut(MessageIn):
    uid: str = Field(default_factory=lambda: str(uuid.uuid4()))


class DirectMessageIn(MessageIn):
    recipient_uid: str


class DirectMessageOut(MessageOut):
    session_general_uid: Optional[str]
    recipient_uid: str


class DirectMessageEdit(BaseModel):
    message_uid: str
    text: str


class GroupMessageEdit(BaseModel):
    message_uid: str
    text: str


class ChannelMessageEdit(BaseModel):
    message_uid: str
    text: str


class GroupMessageIn(MessageIn):
    group_uid: str


class ChannelMessageIn(MessageIn):
    channel_uid: str


class GroupMessageOut(MessageOut):
    group_uid: str


class ChannelMessageOut(MessageOut):
    channel_uid: str


class DeliveryStatus(str, Enum):
    SENT = 'sent'
    DELIVERED = 'delivered'
    READ = 'read'


class DeliveryStatusMessage(BaseModel):
    message_uid: str


class DirectDeliveryStatusMessage(DeliveryStatusMessage):
    status: DeliveryStatus


class GroupDeliveryStatusMessage(DeliveryStatusMessage):
    status: DeliveryStatus


class ChannelDeliveryStatusMessage(DeliveryStatusMessage):
    status: DeliveryStatus
