import os

# Kafka
KAFKA_HOST = os.getenv('KAFKA_HOST', 'kafka')
KAFKA_PORT = os.getenv('KAFKA_PORT', 29092)

# Prometeus
PROMETHEUS_PORT = 9000
