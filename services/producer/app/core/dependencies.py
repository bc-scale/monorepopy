import json
from kafka import KafkaProducer
from enum import Enum
from .config import KAFKA_HOST, KAFKA_PORT


class MessageFlowType(str, Enum):
    DIRECTS = "directs"
    CHANNELS = "channels"
    GROUPS = "groups"
    DIRECTS_EDIT = "directs_edit"
    CHANNELS_EDIT = "channels_edit"
    GROUPS_EDIT = "groups_edit"
    DIRECT_MESSAGE_STATUS = "direct_message_status"
    CHANNEL_MESSAGE_STATUS = "channel_message_status"
    GROUP_MESSAGE_STATUS = "group_message_status"
    DIRECT_MESSAGE_STATUS_UPDATE = "direct_message_status_update"
    CHANNEL_MESSAGE_STATUS_UPDATE = "channel_message_status_update"
    GROUP_MESSAGE_STATUS_UPDATE = "group_message_status_update"


def get_kafka() -> KafkaProducer:
    kafka = KafkaProducer(bootstrap_servers=f'{KAFKA_HOST}:{KAFKA_PORT}',
                          value_serializer=lambda v: json.dumps(v).encode('utf-8'))
    try:
        yield kafka
    finally:
        kafka.close()
    return
