import random
from typing import NamedTuple


class KeyPair(NamedTuple):
    public_key: float
    private_key: float


def gen_key_pair(length_key=128) -> KeyPair:
    return KeyPair(public_key=random.getrandbits(length_key), private_key=random.getrandbits(length_key))


class EncryptionDH(object):
    def __init__(self, public_key1, public_key2, private_key):
        self.public_key1 = public_key1
        self.public_key2 = public_key2
        self.private_key = private_key
        self.full_key = None

    def generate_partial_key(self) -> int:
        partial_key = self.public_key1**self.private_key
        partial_key = partial_key % self.public_key2
        return partial_key

    def generate_full_key(self, partial_key_r) -> int:
        full_key = partial_key_r**self.private_key
        full_key = full_key % self.public_key2
        self.full_key = full_key
        return full_key

    def encrypt_message(self, message) -> str:
        encrypted_message = ""
        key = self.full_key
        for c in message:
            encrypted_message += chr(ord(c)+key)
        return encrypted_message

    def decrypt_message(self, encrypted_message) -> str:
        decrypted_message = ""
        key = self.full_key
        for c in encrypted_message:
            decrypted_message += chr(ord(c)-key)
        return decrypted_message


if __name__ == '__main__':

    # one_person_keys = gen_key_pair(length_key=12)
    # two_person_keys = gen_key_pair(length_key=12)

    one_person_keys = KeyPair(public_key=197, private_key=199)
    two_person_keys = KeyPair(public_key=151, private_key=157)

    Sadat = EncryptionDH(one_person_keys.public_key, two_person_keys.public_key, one_person_keys.private_key)
    Michael = EncryptionDH(one_person_keys.public_key, two_person_keys.public_key, two_person_keys.private_key)

    s_partial = Sadat.generate_partial_key()
    print(s_partial)

    m_partial = Michael.generate_partial_key()
    print(m_partial)

    full_key_m = Michael.generate_full_key(s_partial)
    full_key_s = Sadat.generate_full_key(m_partial)

    print(full_key_m, full_key_s)

    m_encrypted = Michael.encrypt_message('Hi my dear Sabat!')
    print(m_encrypted)

    message = Sadat.decrypt_message(m_encrypted)
    print(message)

