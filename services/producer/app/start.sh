#!/bin/bash

# Kafka
export KAFKA_HOST=kafka
export KAFKA_PORT=29092

uvicorn main:app --reload
