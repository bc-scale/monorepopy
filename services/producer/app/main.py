import uvicorn
from fastapi import FastAPI
from starlette_exporter import PrometheusMiddleware, handle_metrics

from core.logger_elk import logger_elk
from v1.routers import channels, directs, groups

app = FastAPI(title='Producer')
app.add_middleware(PrometheusMiddleware)
app.add_route("/metrics", handle_metrics)

app.include_router(groups.router_groups)
app.include_router(channels.router_channels)
app.include_router(directs.router_directs)

from core.monitoring_metrics import METRIC_PRODUCER_MESSAGES, TIME


@app.get("/")
@TIME
async def root():
    METRIC_PRODUCER_MESSAGES.labels(role='producer', type='MAIN', method='get', endpoint='/').inc()
    logger_elk.info('app producer get /')
    return {"Name service": "Producer"}


if __name__ == '__main__':
    logger_elk.info('Start application producer')
    uvicorn.run(app, host="0.0.0.0", port=8001)
