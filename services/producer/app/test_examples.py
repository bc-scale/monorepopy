from enum import Enum
from typing import NamedTuple
from typing import Literal
from dataclasses import dataclass
import datetime


@dataclass
class User:
    username: str
    created_at: datetime.datetime
    birthday: datetime.datetime | None


class Coordinates(NamedTuple):
    latitude: float
    longitude: float


someone = User(username='Root', created_at=datetime.datetime.now(), birthday=None)

print(someone)
print(someone.username)

someone.username = 'WEWE'
print(someone.username)


class WeatherType(Enum):
    THUNDERSTORM = "Гроза"
    DRIZZLE = "Изморось"
    RAIN = "Дождь"
    SNOW = "Снег"
    CLEAR = "Ясно"
    FOG = "Туман"
    CLOUDS = "Облачно"


print(WeatherType.CLOUDS)

Celsius = int


@dataclass(slots=True, frozen=True)
class Weather:
    temperature: Celsius
    weather_type: WeatherType
    sunrise: datetime
    sunset: datetime
    city: str


for weather_type in WeatherType:
    print(weather_type.name, weather_type.value)

weather_type = WeatherType.RAIN

match weather_type:
    case WeatherType.THUNDERSTORM | WeatherType.RAIN:
        print("Уф, лучше сиди дома")
    case WeatherType.CLEAR:
        print("О, отличная погодка")
    case _:
        print("Ну так, выходить можно")
