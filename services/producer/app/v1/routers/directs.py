from fastapi import APIRouter, Depends
from kafka import KafkaProducer
from starlette import status
from fastapi.encoders import jsonable_encoder
from core.dependencies import get_kafka, MessageFlowType
from core.schemas import message as schemas
from core.monitoring_metrics import METRIC_PRODUCER_MESSAGES, TIME

router_directs = APIRouter(
    prefix="/v1",
    tags=["directs"],
    dependencies=[Depends(get_kafka)],
    responses={404: {"description": "Not found"}},
)


@router_directs.post("/directs", response_model=schemas.DirectMessageOut, status_code=status.HTTP_201_CREATED,)
@TIME
async def read_users(message: schemas.DirectMessageIn, kafka: KafkaProducer = Depends(get_kafka)):
    session_general_uid = f'{message.sender_uid}-{message.recipient_uid}' if message.sender_uid <= message.recipient_uid \
        else f'{message.recipient_uid}-{message.sender_uid}'
    message_out = schemas.DirectMessageOut(**message.dict(), session_general_uid=session_general_uid)
    kafka.send(topic=MessageFlowType.DIRECTS.value, value=jsonable_encoder(message_out), key=session_general_uid.encode())
    kafka.flush()
    METRIC_PRODUCER_MESSAGES.labels(role='producer', type='DIRECTS', method='get', endpoint='/directs').inc()
    return message_out


@router_directs.put("/directs", response_model=schemas.DirectMessageEdit, status_code=status.HTTP_200_OK,)
@TIME
async def edit_users(message: schemas.DirectMessageEdit, kafka: KafkaProducer = Depends(get_kafka)):
    message_out = schemas.DirectMessageEdit(**message.dict())
    kafka.send(topic=MessageFlowType.DIRECTS_EDIT.value, value=jsonable_encoder(message_out), key=message.message_uid.encode())
    kafka.flush()
    METRIC_PRODUCER_MESSAGES.labels(role='producer', type='DIRECTS', method='put', endpoint='/directs').inc()
    return message_out


@router_directs.post("/directs/status", response_model=schemas.DirectDeliveryStatusMessage, status_code=status.HTTP_200_OK,)
@TIME
async def post_status(message: schemas.DirectDeliveryStatusMessage, kafka: KafkaProducer = Depends(get_kafka)):
    message_out = schemas.DirectDeliveryStatusMessage(**message.dict())
    kafka.send(topic=MessageFlowType.DIRECT_MESSAGE_STATUS.value, value=jsonable_encoder(message_out), key=message.message_uid.encode())
    kafka.flush()
    METRIC_PRODUCER_MESSAGES.labels(role='producer', type='DIRECTS', method='post', endpoint='/directs/status').inc()
    return message_out


@router_directs.put("/directs/status", response_model=schemas.DirectDeliveryStatusMessage, status_code=status.HTTP_200_OK,)
@TIME
async def edit_status(message: schemas.DirectDeliveryStatusMessage, kafka: KafkaProducer = Depends(get_kafka)):
    message_out = schemas.DirectDeliveryStatusMessage(**message.dict())
    kafka.send(topic=MessageFlowType.DIRECT_MESSAGE_STATUS_UPDATE.value, value=jsonable_encoder(message_out), key=message.message_uid.encode())
    kafka.flush()
    METRIC_PRODUCER_MESSAGES.labels(role='producer', type='DIRECTS', method='put', endpoint='/directs/status').inc()
    return message_out
