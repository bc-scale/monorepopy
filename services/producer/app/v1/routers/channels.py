from fastapi import APIRouter, Depends
from kafka import KafkaProducer
from starlette import status
from fastapi.encoders import jsonable_encoder
from core.dependencies import get_kafka, MessageFlowType
from core.schemas import message as schemas
from core.monitoring_metrics import METRIC_PRODUCER_MESSAGES, TIME

router_channels = APIRouter(
    prefix="/v1",
    tags=["channels"],
    dependencies=[Depends(get_kafka)],
    responses={404: {"description": "Not found"}},
)


@router_channels.post("/channels", response_model=schemas.ChannelMessageOut, status_code=status.HTTP_201_CREATED,)
@TIME
async def read_users(message: schemas.ChannelMessageIn, kafka: KafkaProducer = Depends(get_kafka)):
    message_out = schemas.ChannelMessageOut(**message.dict())
    kafka.send(topic=MessageFlowType.CHANNELS.value, value=jsonable_encoder(message_out),
               key=message_out.channel_uid.encode())
    kafka.flush()
    METRIC_PRODUCER_MESSAGES.labels(role='producer', type='CHANNELS', method='get', endpoint='/channels').inc()
    return message_out


@router_channels.put("/channels", response_model=schemas.ChannelMessageEdit, status_code=status.HTTP_200_OK,)
@TIME
async def edit_users(message: schemas.ChannelMessageEdit, kafka: KafkaProducer = Depends(get_kafka)):
    message_out = schemas.ChannelMessageEdit(**message.dict())
    kafka.send(topic=MessageFlowType.CHANNELS_EDIT.value, value=jsonable_encoder(message_out), key=message.message_uid.encode())
    kafka.flush()
    METRIC_PRODUCER_MESSAGES.labels(role='producer', type='CHANNELS', method='put', endpoint='/channels').inc()
    return message_out


@router_channels.post("/channels/status", response_model=schemas.ChannelDeliveryStatusMessage, status_code=status.HTTP_200_OK,)
@TIME
async def post_status(message: schemas.ChannelDeliveryStatusMessage, kafka: KafkaProducer = Depends(get_kafka)):
    message_out = schemas.ChannelDeliveryStatusMessage(**message.dict())
    kafka.send(topic=MessageFlowType.CHANNEL_MESSAGE_STATUS.value, value=jsonable_encoder(message_out), key=message.message_uid.encode())
    kafka.flush()
    METRIC_PRODUCER_MESSAGES.labels(role='producer', type='CHANNELS', method='post', endpoint='/channels/status').inc()
    return message_out


@router_channels.put("/channels/status", response_model=schemas.ChannelDeliveryStatusMessage, status_code=status.HTTP_200_OK,)
@TIME
async def edit_status(message: schemas.ChannelDeliveryStatusMessage, kafka: KafkaProducer = Depends(get_kafka)):
    message_out = schemas.ChannelDeliveryStatusMessage(**message.dict())
    kafka.send(topic=MessageFlowType.CHANNEL_MESSAGE_STATUS_UPDATE.value, value=jsonable_encoder(message_out), key=message.message_uid.encode())
    kafka.flush()
    METRIC_PRODUCER_MESSAGES.labels(role='producer', type='CHANNELS', method='put', endpoint='/channels/status').inc()
    return message_out
