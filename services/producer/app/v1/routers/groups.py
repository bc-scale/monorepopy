import uuid

from fastapi import APIRouter, Depends
from kafka import KafkaProducer
from starlette import status
from fastapi.encoders import jsonable_encoder
from core.dependencies import get_kafka, MessageFlowType
from core.schemas import message as schemas
from core.monitoring_metrics import METRIC_PRODUCER_MESSAGES, TIME

router_groups = APIRouter(
    prefix="/v1",
    tags=["groups"],
    dependencies=[Depends(get_kafka)],
    responses={404: {"description": "Not found"}},
)


@router_groups.post("/groups", response_model=schemas.GroupMessageOut, status_code=status.HTTP_201_CREATED)
@TIME
async def read_users(message: schemas.GroupMessageIn, kafka: KafkaProducer = Depends(get_kafka)):
    message_out = schemas.GroupMessageOut(**message.dict())
    kafka.send(topic=MessageFlowType.GROUPS.value, value=jsonable_encoder(message_out), key=message_out.group_uid.encode())
    kafka.flush()
    METRIC_PRODUCER_MESSAGES.labels(role='producer', type='GROUPS', method='get', endpoint='/groups').inc()
    return message_out


@router_groups.put("/groups", response_model=schemas.GroupMessageEdit, status_code=status.HTTP_200_OK,)
@TIME
async def edit_users(message: schemas.GroupMessageEdit, kafka: KafkaProducer = Depends(get_kafka)):
    message_out = schemas.GroupMessageEdit(**message.dict())
    kafka.send(topic=MessageFlowType.GROUPS_EDIT.value, value=jsonable_encoder(message_out), key=message.message_uid.encode())
    kafka.flush()
    METRIC_PRODUCER_MESSAGES.labels(role='producer', type='GROUPS', method='put', endpoint='/groups').inc()
    return message_out


@router_groups.post("/groups/status", response_model=schemas.GroupDeliveryStatusMessage, status_code=status.HTTP_200_OK,)
@TIME
async def post_status(message: schemas.GroupDeliveryStatusMessage, kafka: KafkaProducer = Depends(get_kafka)):
    message_out = schemas.GroupDeliveryStatusMessage(**message.dict())
    kafka.send(topic=MessageFlowType.GROUP_MESSAGE_STATUS.value, value=jsonable_encoder(message_out), key=message.message_uid.encode())
    kafka.flush()
    METRIC_PRODUCER_MESSAGES.labels(role='producer', type='GROUPS', method='post', endpoint='/groups/status').inc()
    return message_out


@router_groups.put("/groups/status", response_model=schemas.GroupDeliveryStatusMessage, status_code=status.HTTP_200_OK,)
@TIME
async def edit_status(message: schemas.GroupDeliveryStatusMessage, kafka: KafkaProducer = Depends(get_kafka)):
    message_out = schemas.GroupDeliveryStatusMessage(**message.dict())
    kafka.send(topic=MessageFlowType.GROUP_MESSAGE_STATUS_UPDATE.value, value=jsonable_encoder(message_out), key=message.message_uid.encode())
    kafka.flush()
    METRIC_PRODUCER_MESSAGES.labels(role='producer', type='GROUPS', method='put', endpoint='/groups/status').inc()
    return message_out
