from core import dependencies


def test_kafka_connection():
    assert dependencies.get_kafka() is not None
