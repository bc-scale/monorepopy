import sys
from os.path import dirname
sys.path.append(dirname(dirname(dirname(__file__))))

from fastapi.testclient import TestClient

from main import app

client = TestClient(app)


def test_read_main():
    response = client.get('/')
    assert response.status_code == 200
    assert response.json() == {"Name service": "Producer"}


def test_directs_post_message():
    data = {
        "sender_uid": "string",
        "text": "new message 3",
        "recipient_uid": "string",
    }
    response = client.post('/v1/directs', json=data)
    assert response.status_code == 201
    response_data = response.json()
    assert response_data['sender_uid'] == 'string'
    assert response_data['text'] == 'new message 3'
    assert response_data['uid'] is not None
    assert response_data['session_general_uid'] is not None
    assert response_data['recipient_uid'] == 'string'


def test_directs_put_message():
    data = {
        "message_uid": "1234567890",
        "text": "new text message",
    }
    response = client.put('/v1/directs', json=data)
    assert response.status_code == 200
    response_data = response.json()
    assert response_data['message_uid'] == '1234567890'
    assert response_data['text'] == 'new text message'


def test_groups_post():
    data = {
        "sender_uid": "sender",
        "text": "new",
        "group_uid": "id_group"
    }
    response = client.post('/v1/groups', json=data)
    assert response.status_code == 201
    response_data = response.json()
    assert response_data['sender_uid'] == 'sender'
    assert response_data['text'] == 'new'
    assert response_data['uid'] is not None
    assert response_data['group_uid'] == 'id_group'


def test_groups_put():
    data = {
        "message_uid": "string",
        "text": "string"
    }
    response = client.put('/v1/groups', json=data)
    assert response.status_code == 200
    response_data = response.json()
    assert response_data['message_uid'] == 'string'
    assert response_data['text'] == 'string'


def test_channels_post():
    data = {
        "sender_uid": "string",
        "text": "string",
        "channel_uid": "id_channel",
    }
    response = client.post('/v1/channels', json=data)
    assert response.status_code == 201
    response_data = response.json()
    assert response_data['sender_uid'] == 'string'
    assert response_data['text'] == 'string'
    assert response_data['uid'] is not None
    assert response_data['channel_uid'] == 'id_channel'


def test_channels_put():
    data = {
        "message_uid": "string",
        "text": "string"
    }
    response = client.put('/v1/channels', json=data)
    assert response.status_code == 200
    response_data = response.json()
    assert response_data['message_uid'] == 'string'
    assert response_data['text'] == 'string'


def test_channels_post_status():
    data = {
        "message_uid": "string",
        "status": "sent",
    }
    response = client.post('/v1/channels/status', json=data)
    assert response.status_code == 200
    response_data = response.json()
    assert response_data['message_uid'] == 'string'
    assert response_data['status'] == 'sent'


def test_channels_put_status():
    data = {
        "message_uid": "string",
        "status": "sent",
    }
    response = client.put('/v1/channels/status', json=data)
    assert response.status_code == 200
    response_data = response.json()
    assert response_data['message_uid'] == 'string'
    assert response_data['status'] == 'sent'


def test_direct_post_status():
    data = {
        "message_uid": "string",
        "status": "sent",
    }
    response = client.post('/v1/direct/status', json=data)
    assert response.status_code == 404, response.text
    assert response.json() == {"detail":"Not Found"}


def test_direct_put_status():
    data = {
        "message_uid": "string",
        "status": "sent",
    }
    response = client.put('/v1/direct/status', json=data)
    assert response.status_code == 404, response.text
    assert response.json() == {"detail":"Not Found"}


def test_groups_post_status():
    data = {
        "message_uid": "string",
        "status": "sent",
    }
    response = client.post('/v1/groups/status', json=data)
    assert response.status_code == 200, response.text
    response_data = response.json()
    assert response_data['message_uid'] == 'string'
    assert response_data['status'] == 'sent'


def test_groups_put_status():
    data = {
        "message_uid": "string",
        "status": "sent",
    }
    response = client.put('/v1/groups/status', json=data)
    assert response.status_code == 200, response.text
    response_data = response.json()
    assert response_data['message_uid'] == 'string'
    assert response_data['status'] == 'sent'

