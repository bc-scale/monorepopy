from pydantic import BaseModel


class UserCreate(BaseModel):
    """Проверка Sing-up"""
    phone: str
    password: str


class UserBase(BaseModel):
    """Ответ с деталями"""
    id: int
    phone: str


class Token(BaseModel):
    access_token: str
    refresh_token: str
