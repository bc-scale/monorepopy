import os
import jwt

from fastapi import HTTPException

from models.database import database
from models.users import user_table
from schemas import users as user_schema
from utils.security import get_password_hash


async def get_user_by_phone(phone: str):
    """Возвращаем информацию о пользователе"""
    query = user_table.select().where(user_table.c.phone == phone)
    return await database.fetch_one(query)


async def create_user(user: user_schema.UserCreate):
    """Создаем нового пользователя в БД"""
    hashed_password = get_password_hash(user.password)
    query = user_table.insert().values(
        phone=user.phone,
        password=hashed_password,
    )
    await database.execute(query)

#
# async def get_user_by_token(token: str):
#     try:
#         payload = jwt.decode(token, os.environ.get('SECRET_KEY'), algorithms=['HS256'])
#     except jwt.ExpiredSignatureError:
#         raise HTTPException(status_code=401, detail='Token has expired')
#
#     query = user_table.select().where(user_table.c.id == payload['user_id'])
#     return await database.fetch_one(query)
