from utils import users as users_utils
from fastapi import Depends, HTTPException, status
from fastapi.security import OAuth2PasswordBearer

from core.logger_elk import logger_elk

oath2_schema = OAuth2PasswordBearer(tokenUrl='/login')


async def get_current_user(token: str = Depends(oath2_schema)):
    user = await users_utils.get_user_by_token(token)
    if not user:
        logger_elk.error(f'Invalid authentication credentials')
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail='Invalid authentication credentials',
            headers={'WWW-Authenticate': 'Bearer'},
        )
    if not user['is_active']:
        logger_elk.error(f'Inactive user')
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail='Inactive user'
        )
    return user
