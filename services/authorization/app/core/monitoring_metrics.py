from prometheus_async.aio import time
from prometheus_client import Counter, Histogram

METRIC_REQUEST_CALL_TIMES = Counter('requests_call_total', 'HTTP requests', ['task_or_results', 'type_operation'])

# Exceptions
METRIC_AUTH_EXCEPTIONS = Counter('requests_auth_exceptions_total', 'Count of exceptions', ['code', 'detail'])

# Time requests
# REQUEST_TIME = Summary('request_auth_processing_seconds', 'Time spent processing request for authorization', )
REQ_TIME = Histogram("req_time_seconds", "time spent in requests")
TIME = time(REQ_TIME)

# Routs
METRIC_AUTH_ROUTS = Counter('requests_auth_routs_total', 'Count auth requests', ['role', 'type', 'method', 'endpoint'])

# create_account_timer = REQUEST_TIME.labels(method='post', endpoint='/accounts')
