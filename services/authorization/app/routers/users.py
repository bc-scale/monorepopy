import os
import re
from datetime import timedelta
from pydantic import BaseModel

from fastapi import APIRouter, HTTPException
from fastapi import Depends
from fastapi_jwt_auth import AuthJWT
from fastapi_jwt_auth.exceptions import JWTDecodeError

from schemas import users
from utils import users as users_utils

from utils.security import get_password_hash, verify_password
from core.logger_elk import logger_elk
from core.monitoring_metrics import METRIC_AUTH_ROUTS

router = APIRouter()


EXPIRES_ACCESS_TOKEN = timedelta(seconds=60)
EXPIRES_REFRESH_TOKEN = timedelta(days=1)
SECRET_KEY = os.environ.get('SECRET_KEY')


class Settings(BaseModel):
    authjwt_secret_key: str = SECRET_KEY
    authjwt_decode_algorithms: set = {'HS256', "HS384", "HS512"}


@AuthJWT.load_config
def get_config():
    return Settings()


@router.post('/signup', response_model=users.UserBase, tags=['Authorization'])
async def create_user(user: users.UserCreate):
    regex = r'[87]\d{,11}$'
    if not re.fullmatch(regex, user.phone):
        raise HTTPException(status_code=400, detail='Phone is not valid')

    db_user = await users_utils.get_user_by_phone(phone=user.phone)
    if db_user:
        logger_elk.error(f'User {user} is registered')
        raise HTTPException(status_code=400, detail='User is registered')
    METRIC_AUTH_ROUTS.labels(role='auth', type='USERS', method='post', endpoint='/signup').inc()
    return await users_utils.create_user(user=user)


@router.post('/login', response_model=users.Token, tags=['Authorization'])
async def login(user: users.UserCreate, Authorize: AuthJWT = Depends()):
    user_db = await users_utils.get_user_by_phone(phone=user.phone)
    if not user_db:
        logger_elk.error(f'User or password incorrect')
        raise HTTPException(status_code=400, detail='User or password incorrect')
    elif not user_db.is_active:
        logger_elk.error(f'Inactive user')
        raise HTTPException(status_code=400, detail='Inactive user')

    if not verify_password(plain_password=user.password, hashed_password=user_db["password"]):
        logger_elk.error(f'User or password incorrect')
        raise HTTPException(status_code=400, detail='User or password incorrect')
    METRIC_AUTH_ROUTS.labels(role='auth', type='USERS', method='post', endpoint='/login').inc()
    access_token = Authorize.create_access_token(subject=user.phone, algorithm="HS256",
                                                 expires_time=EXPIRES_ACCESS_TOKEN)
    refresh_token = Authorize.create_refresh_token(subject=user.phone, algorithm="HS256",
                                                   expires_time=EXPIRES_REFRESH_TOKEN)
    return {"access_token": access_token, "refresh_token": refresh_token}


@router.post('/refresh', response_model=users.Token, tags=['Authorization'])
def refresh(Authorize: AuthJWT = Depends()):

    Authorize.jwt_refresh_token_required()

    current_user = Authorize.get_jwt_subject()

    new_access_token = Authorize.create_access_token(subject=current_user, algorithm="HS256",
                                                     expires_time=EXPIRES_ACCESS_TOKEN)
    new_refresh_token = Authorize.create_refresh_token(subject=current_user, algorithm="HS256",
                                                       expires_time=EXPIRES_REFRESH_TOKEN)
    return {"access_token": new_access_token, "refresh_token": new_refresh_token}


@router.get('/protected', tags=['Authorization'])
def protected(Authorize: AuthJWT = Depends()):

    try:
        Authorize.jwt_required()
    except JWTDecodeError as err:
        status_code = err.status_code
        if err.message == "Signature verification failed":
            status_code = 401
        raise HTTPException(status_code=status_code, detail=err.message)

    current_user = Authorize.get_jwt_subject()
    return {"user": current_user}
