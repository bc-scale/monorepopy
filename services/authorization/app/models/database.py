import os

import databases

DB_USER = os.environ.get('DB_USER', 'postgres')
DB_PASS = os.environ.get('DB_PASS', 'postgres')
DB_HOST = os.environ.get('DB_HOST', 'postgres')
DB_PORT = os.environ.get('DB_PORT', '5432')
DB_NAME = os.environ.get('DB_NAME', 'postgres')
TESTING = os.environ.get('TESTING')

if TESTING:
    DB_NAME = 'wave_test'
    TEST_SQLALCHEMY_DATABASE_URL = (
        f'postgresql://{DB_USER}:{DB_PASS}@{DB_HOST}:{DB_PORT}/{DB_NAME}'
    )
    database = databases.Database(TEST_SQLALCHEMY_DATABASE_URL)
else:
    SQLALCHEMY_DATABASE_URL = (
        f'postgresql+asyncpg://{DB_USER}:{DB_PASS}@{DB_HOST}:{DB_PORT}/{DB_NAME}'
    )
    database = databases.Database(SQLALCHEMY_DATABASE_URL)
