import sqlalchemy

metadata = sqlalchemy.MetaData()

user_table = sqlalchemy.Table(
    'users',
    metadata,
    sqlalchemy.Column('id', sqlalchemy.Integer, primary_key=True),
    sqlalchemy.Column('phone', sqlalchemy.String(12), unique=True, index=True),
    sqlalchemy.Column('password', sqlalchemy.String(100)),
    sqlalchemy.Column(
        'is_active',
        sqlalchemy.Boolean(),
        server_default=sqlalchemy.sql.expression.true(),
        nullable=False
    ),
)
