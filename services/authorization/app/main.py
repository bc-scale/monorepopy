import sys
import uvicorn

from fastapi import FastAPI
from starlette_exporter import PrometheusMiddleware, handle_metrics

from os.path import dirname
sys.path.append(dirname(dirname(__file__)))
sys.path.append(dirname(__file__))

from routers import users
from models.database import database
from core.logger_elk import logger_elk
from core.monitoring_metrics import METRIC_AUTH_ROUTS, TIME


app = FastAPI()

app.add_middleware(PrometheusMiddleware)
app.add_route("/metrics", handle_metrics)

app.include_router(users.router)


@app.get("/")
@TIME
async def root():
    METRIC_AUTH_ROUTS.labels(role='auth', type='MAIN', method='get', endpoint='/').inc()
    logger_elk.debug('app messages authorization get /')
    return {"Name service": "Authorization"}


@app.on_event('startup')
async def startup():
    await database.connect()


@app.on_event('shutdown')
async def shutdown():
    await database.disconnect()


if __name__ == "__main__":
    logger_elk.info('Start application authorization')
    uvicorn.run(app, host="0.0.0.0", port=8000)
