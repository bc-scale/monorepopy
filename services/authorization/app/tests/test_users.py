import sys
from os.path import dirname
sys.path.append(dirname(dirname(__file__)))
sys.path.append(dirname(dirname(dirname(__file__))))

from fastapi.testclient import TestClient

from main import app

client = TestClient(app)


def test_main_page():
    response = client.get('/')
    assert response.status_code == 200, response.status_code
    assert response.json() == {"Name service": "Authorization"}


def test_sing_up(temp_db):
    request_data = {
        'phone': '77017520540',
        'password': 'password'
    }
    with TestClient(app) as client:
        response = client.post('/signup', json=request_data)
    assert response.status_code == 200, response.status_code


def test_sing_up_registered_user(temp_db):
    request_data = {
        'phone': '77017520540',
        'password': 'password'
    }
    with TestClient(app) as client:
        response = client.post('/signup', json=request_data)
    assert response.status_code == 400, response.status_code
    assert response.json() == {"detail": "User is registered"}


def test_login(temp_db):
    request_data = {'phone': '77017520540', 'password': 'password'}
    with TestClient(app) as client:
        response = client.post('/login', json=request_data)
    assert response.status_code == 200, response.text
    assert response.json()['access_token'] is not None
    assert response.json()['refresh_token'] is not None


def test_login_with_invalid_password(temp_db):
    request_data = {'phone': '77017520540', 'password': 'error-password'}
    with TestClient(app) as client:
        response = client.post('/login', json=request_data)
    assert response.status_code == 400
    assert response.json()['detail'] == 'User or password incorrect'


def test_login_with_invalid_phone(temp_db):
    request_data = {'phone': '17017520540', 'password': 'password'}
    with TestClient(app) as client:
        response = client.post('/login', json=request_data)
    assert response.status_code == 400
    assert response.json()['detail'] == 'User or password incorrect'


def test_user_refresh_token(temp_db):
    request_data = {'phone': '77017520540', 'password': 'password'}
    with TestClient(app) as client:
        response = client.post('/login', json=request_data)
        token = response.json()['refresh_token']
        headers = {f'Authorization': f'Bearer {token}'}
    with TestClient(app) as client:
        response = client.post('/refresh', headers=headers)

    assert response.status_code == 200, response.text
    assert response.json()['access_token'] is not None
    assert response.json()['refresh_token'] is not None


def test_user_protected_page_with_token(temp_db):
    request_data = {'phone': '77017520540', 'password': 'password'}
    with TestClient(app) as client:
        response = client.post('/login', json=request_data)
        token = response.json()['access_token']
        headers = {f'Authorization': f'Bearer {token}'}
    with TestClient(app) as client:
        response = client.get('/protected', headers=headers)

    assert response.status_code == 200, response.text
    assert response.json()['user'] == '77017520540'


def test_user_protected_page_with_broken_token(temp_db):
    headers = {f'Authorization': f'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiI3NzAxN'
                                 f'zUyMDU0OSIsImlhdCI6MTY2Mzk0NTE0MSwibmJmIjoxNjYzOTQ1MTQxLCJqdGki'
                                 f'OiIwZDU5ZGM0Yy1mYzgxLTQ5MjktODZmYy1jN2ExYWRmYTUyYzIiLCJleHAiOjE2N'
                                 f'jM5NDUyMDEsInR5cGUiOiJhY2Nlc3MiLCJmcmVzaCI6ZmFsc2V9.eajE3Snw7tsR2p03'
                                 f'OENbjqM0_LJNIq3FCGRAHVBpZKY'}
    with TestClient(app) as client:
        response = client.get('/protected', headers=headers)

    assert response.status_code == 401, response.text
    assert response.json() == {"detail": "Signature verification failed"}
