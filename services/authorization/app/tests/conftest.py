import os
import sys
from os.path import dirname
sys.path.append(dirname(dirname(__file__)))
sys.path.append(dirname(dirname(dirname(__file__))))

import pytest

os.environ['TESTING'] = 'True'

from alembic import command
from alembic.config import Config
from models import database
from sqlalchemy_utils import create_database, drop_database


@pytest.fixture(scope='module')
def temp_db():
    create_database(database.TEST_SQLALCHEMY_DATABASE_URL)
    base_dir = os.path.dirname(os.path.dirname(os.path.dirname(__file__)))
    alembic_cfg = Config(os.path.join(base_dir, 'alembic.ini'))
    command.upgrade(alembic_cfg, 'head')

    try:
        yield database.TEST_SQLALCHEMY_DATABASE_URL
    finally:
        drop_database(database.TEST_SQLALCHEMY_DATABASE_URL)
