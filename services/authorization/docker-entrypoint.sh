#!/usr/bin/env sh


$counter
until alembic upgrade head
do
    echo "Waiting for db to be ready..."
    if "$counter" -gt 5;
    then
        echo "Exiting loop!"
        exit 2
    else
        counter=$((counter+1))
        sleep 1
    fi
done


uvicorn app.main:app --host 0.0.0.0 --port 8081 --reload
