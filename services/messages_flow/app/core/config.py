import os

# MongoDB
MONGODB_HOST = os.getenv('FLOW_MONGODB_HOST', 'localhost')
MONGODB_PORT = os.getenv('FLOW_MONGODB_PORT', 27017)
MONGODB_USER = os.getenv('FLOW_MONGODB_USER', 'root')
MONGODB_PASSWORD = os.getenv('FLOW_MONGODB_PASSWORD', 'example')
