from .interfaces import MessageStorage
from .logger_elk import logger_elk
from .models import Message, DeliveryStatusMessage, DirectMessage
from pymongo import MongoClient
from .config import MONGODB_HOST, MONGODB_PORT, MONGODB_USER, MONGODB_PASSWORD


class MongoStorage(MessageStorage):

    def __init__(self):
        self.client = MongoClient(f'mongodb://{MONGODB_USER}:{MONGODB_PASSWORD}@{MONGODB_HOST}:{MONGODB_PORT}')
        self.database_name = 'messages'

    # def save_message(self, message: Message) -> None:
    #
    #     match message.__class__.__name__:
    #         case 'DirectMessage':
    #             db = self.client.messages.direct_messages
    #         case 'GroupMessage':
    #             db = self.client.messages.group_messages
    #         case 'ChannelMessage':
    #             db = self.client.messages.channel_messages
    #         case _:
    #             logger_elk.exception("Unknown Message Type")
    #             raise ValueError("Unknown Message Type")
    #
    #     db.insert_one(message.dict())

    # def save_delivery_status(self, delivery_status: DeliveryStatusMessage) -> None:
    #     db = self.client.delivery_statuses
    #     db.insert_one(delivery_status)

    # def clear_collection(self, collection_name: str) -> int:
    #     if collection_name in self.client.messages.list_collections():
    #         pass

    # TODO реализовать метод поиска на дженериках
    # def find(self, request):
    #     return self.client.messages

    def get_db(self):
        return self.client.messages

