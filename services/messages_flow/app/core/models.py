from enum import Enum
from pydantic import BaseModel


class Message(BaseModel):
    uid: str
    sender_uid: str
    text: str


class DirectMessage(Message):
    session_general_uid: str | None = None
    recipient_uid: str


class GroupMessage(Message):
    group_uid: str


class ChannelMessage(Message):
    channel_uid: str


class DeliveryStatus(str, Enum):
    SENT = 'sent'
    DELIVERED = 'delivered'
    READ = 'read'


class DeliveryStatusMessage(BaseModel):
    message_uid: str
    status: DeliveryStatus
