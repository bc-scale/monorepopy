from typing import Protocol
from .models import Message, DeliveryStatusMessage


class MessageStorage(Protocol):
    """Interface for any storage saving message"""
    def save_message(self, message: Message) -> None:
        raise NotImplementedError

    def save_delivery_status(self, delivery_status: DeliveryStatusMessage) -> None:
        raise NotImplementedError
