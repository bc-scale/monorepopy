import json
from kafka import KafkaProducer
from enum import Enum
from .storages import MongoStorage


class MessageFlowType(str, Enum):
    DIRECTS = "directs"
    CHANNELS = "channels"
    GROUPS = "groups"
    DIRECTS_EDIT = "directs_edit"
    CHANNELS_EDIT = "channels_edit"
    GROUPS_EDIT = "groups_edit"


def get_mongodb() -> MongoStorage:
    mongo_storage = MongoStorage()
    try:
        yield mongo_storage
    finally:
        mongo_storage.client.close()
    return
