import sys
from os.path import dirname
sys.path.append(dirname(dirname(dirname(__file__))))

from pymongo import MongoClient
from fastapi.testclient import TestClient

from core import dependencies, interfaces
from main import app
from core.config import MONGODB_HOST, MONGODB_PORT, MONGODB_USER, MONGODB_PASSWORD

client = TestClient(app)


def test_read_main():
    response = client.get('/')
    assert response.status_code == 200
    assert response.json() == {"Name service": "Messages Flow"}


def test_mongodb_connect():
    assert dependencies.get_mongodb() is not None


def test_read_direct():
    # write test data
    client_write_message = MongoClient(f'mongodb://{MONGODB_USER}:{MONGODB_PASSWORD}@{MONGODB_HOST}:{MONGODB_PORT}')
    data = {'uid': '35d0b330-8386-41a1-a911-17e951ad4adc', 'sender_uid': 'string', 'text': 'string',
            'session_general_uid': 'string-string', 'recipient_uid': 'string'}
    db = client_write_message['messages']
    series_collection = db['direct_messages']
    series_collection.insert_one(data)

    response = client.get(f'v1/directs/string-string')

    assert response.status_code == 200
    assert len(response.json()) != 0
    data = response.json()
    for item in data:
        assert len(item) == 5
        assert all([len(value) for value in item.values()])


def test_read_channels():
    client_write_message = MongoClient(f'mongodb://{MONGODB_USER}:{MONGODB_PASSWORD}@{MONGODB_HOST}:{MONGODB_PORT}')
    data = {'uid': '35d0b330-8386-41a1-a911-17e951ad4adc', 'sender_uid': 'string', 'text': 'string',
            'channel_uid': 'string-string'}
    db = client_write_message['messages']
    series_collection = db['channels_messages']
    series_collection.insert_one(data)

    response = client.get(f'v1/channels/string-string')

    assert response.status_code == 200, response.text


def test_read_groups():
    client_write_message = MongoClient(f'mongodb://{MONGODB_USER}:{MONGODB_PASSWORD}@{MONGODB_HOST}:{MONGODB_PORT}')
    data = {'uid': '35d0b330-8386-41a1-a911-17e951ad4adc', 'sender_uid': 'string', 'text': 'string',
            'channel_uid': 'string-string'}
    db = client_write_message['messages']
    series_collection = db['channels_messages']
    series_collection.insert_one(data)

    response = client.get(f'v1/groups/string-string')

    assert response.status_code == 200, response.text
