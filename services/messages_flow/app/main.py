import os
import uvicorn
from fastapi import FastAPI

from starlette_exporter import PrometheusMiddleware, handle_metrics

from v1.routers import channels, directs, groups
from core.logger_elk import logger_elk
from core.monitoring_metrics import METRIC_FLOW_MESSAGES, TIME

app = FastAPI(title='Messages Flow')

app.add_middleware(PrometheusMiddleware)
app.add_route("/metrics", handle_metrics)

app.include_router(groups.router_groups)
app.include_router(channels.router_channels)
app.include_router(directs.router_directs)


@app.get("/")
@TIME
async def root():
    logger_elk.debug('app messages flow get /')
    METRIC_FLOW_MESSAGES.labels(role='flow', type='MAIN', method='get', endpoint='/').inc()
    return {"Name service": "Messages Flow"}


if __name__ == '__main__':
    logger_elk.info('Start application messages flow')
    uvicorn.run(app, host="0.0.0.0", port=8001)
