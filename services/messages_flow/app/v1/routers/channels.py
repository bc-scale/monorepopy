from fastapi import APIRouter, Depends
from starlette import status
from core.dependencies import get_mongodb
from core.schemas import message as schemas
from core.storages import MongoStorage
from core.monitoring_metrics import METRIC_FLOW_MESSAGES, TIME

router_channels = APIRouter(
    prefix="/v1",
    tags=["channels"],
    dependencies=[Depends(get_mongodb)],
    responses={404: {"description": "Not found"}},
)


@router_channels.get("/channels/{channel_uid}", response_model=list[schemas.ChannelMessage],
                    status_code=status.HTTP_200_OK,)
@TIME
async def read_channels(channel_uid: str, mongo_storage: MongoStorage = Depends(get_mongodb)):
    messages_out = mongo_storage.get_db().channel_messages.find({"channel_uid": channel_uid})
    messages_out = map(lambda m: schemas.ChannelMessage(**m), messages_out)
    METRIC_FLOW_MESSAGES.labels(role='flow', type='CHANNELS', method='get', endpoint='/channels/{channel_uid}').inc()
    return list(messages_out)
