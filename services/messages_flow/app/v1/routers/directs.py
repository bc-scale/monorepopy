from fastapi import APIRouter, Depends
from starlette import status
from core.dependencies import get_mongodb
from core.schemas import message as schemas
from core.storages import MongoStorage
from core.monitoring_metrics import METRIC_FLOW_MESSAGES, TIME

router_directs = APIRouter(
    prefix="/v1",
    tags=["directs"],
    dependencies=[Depends(get_mongodb)],
    responses={404: {"description": "Not found"}},
)


@router_directs.get("/directs/{session_general_uid}", response_model=list[schemas.DirectMessage],
                    status_code=status.HTTP_200_OK,)
@TIME
async def read_directs(session_general_uid: str, mongo_storage: MongoStorage = Depends(get_mongodb)):
    messages_out = mongo_storage.get_db().direct_messages.find({"session_general_uid": session_general_uid})
    messages_out = map(lambda m: schemas.DirectMessage(**m), messages_out)
    METRIC_FLOW_MESSAGES.labels(role='flow', type='DIRECTS', method='get', endpoint='/directs/{session_general_uid}').inc()

    return list(messages_out)
