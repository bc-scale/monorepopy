from fastapi import APIRouter, Depends
from starlette import status
from core.dependencies import get_mongodb
from core.schemas import message as schemas
from core.storages import MongoStorage
from core.monitoring_metrics import METRIC_FLOW_MESSAGES, TIME


router_groups = APIRouter(
    prefix="/v1",
    tags=["groups"],
    dependencies=[Depends(get_mongodb)],
    responses={404: {"description": "Not found"}},
)


@router_groups.get("/groups/{group_uid}", response_model=list[schemas.GroupMessage],
                    status_code=status.HTTP_200_OK,)
@TIME
async def read_groups(group_uid: str, mongo_storage: MongoStorage = Depends(get_mongodb)):
    messages_out = mongo_storage.get_db().group_messages.find({"group_uid": group_uid})
    messages_out = map(lambda m: schemas.GroupMessage(**m), messages_out)
    METRIC_FLOW_MESSAGES.labels(role='flow', type='GROUPS', method='get', endpoint='/groups/{group_uid}').inc()
    return list(messages_out)
