import os

# Postgres
POSTGRES_HOST = os.getenv('ACCOUNT_DB_HOST', 'postgres-db')
POSTGRES_PORT = os.getenv('ACCOUNT_DB_PORT', 5432)
POSTGRES_DB_NAME = os.getenv('ACCOUNT_DB_NAME', 'accounts')
POSTGRES_USER = os.getenv('ACCOUNT_DB_USER', 'postgres_admin')
POSTGRES_PASSWORD = os.getenv('ACCOUNT_DB_PASSWORD', 'Eethaxie9xe9aeni')
CERT_PATH = os.getenv('CERT_PATH', '')
SSL_MODE = os.getenv('SSL_MODE', 'verify-full')

# Secret key fo JWT
SECRET_KEY = os.environ.get('SECRET_KEY')
