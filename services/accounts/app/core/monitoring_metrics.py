from prometheus_async.aio import time
from prometheus_client import Counter, Summary, Histogram

METRIC_REQUEST_CALL_TIMES = Counter('requests_call_total', 'HTTP requests', ['task_or_results', 'type_operation'])

# Requests
METRIC_ACCOUNTS_REQUESTS = Counter('requests_accounts_total', 'HTTP requests', ['method', 'endpoint'])

# Exceptions
METRIC_ACCOUNTS_EXCEPTIONS = Counter('requests_accounts_exceptions_total', 'Count of exceptions', ['code', 'detail'])

# Time requests
# REQUEST_TIME = Summary('request_accounts_processing_seconds', 'Time spent processing request for accounts', )
REQ_TIME = Histogram("req_time_seconds", "time spent in requests")
TIME = time(REQ_TIME)

# create_account_timer = REQUEST_TIME.labels(method='post', endpoint='/accounts')
