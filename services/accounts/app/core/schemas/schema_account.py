import uuid
from pydantic import BaseModel
from uuid import UUID
from typing import Optional


class GroupBase(BaseModel):
    title: str
    description: str | None = None
    domain: str = 'public'


class GroupCreate(GroupBase):
    pass


class Group(GroupBase):
    uid: UUID = uuid.uuid4()
    owner_id: UUID

    class Config:
        orm_mode = True


class ChannelBase(BaseModel):
    title: str
    description: str | None = None
    domain: str = 'public'


class ChannelCreate(ChannelBase):
    pass


class Channel(ChannelBase):
    uid: UUID = uuid.uuid4()
    owner_id: UUID

    class Config:
        orm_mode = True


class AccountBase(BaseModel):
    email: str
    phone: str


class AccountCreate(AccountBase):
    password: str


class Account(AccountBase):
    uid: UUID = uuid.uuid4()
    is_active: bool = True
    email: str
    phone: str
    groups: list[Group] = []
    channels: list[Channel] = []

    class Config:
        orm_mode = True


class AuthAccount(BaseModel):
    phone: Optional[str]
    password: Optional[str]


class Token(BaseModel):
    access_token: str
    refresh_token: str
