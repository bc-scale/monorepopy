import uuid

from sqlalchemy.orm import Session
from .models import account as account_model
from .schemas import schema_account
from .logger_elk import log_error


@log_error
def get_account(db: Session, account_uid: int):
    return db.query(account_model.Account).filter(account_model.Account.uid == account_uid).first()


@log_error
def get_account_by_email(db: Session, email: str):
    return db.query(account_model.Account).filter(account_model.Account.email == email).first()


@log_error
def get_accounts(db: Session, skip: int = 0, limit: int = 100):
    return db.query(account_model.Account).offset(skip).limit(limit).all()


@log_error
def create_account(db: Session, account: schema_account.AccountCreate):
    fake_hashed_password = account.password + "notreallyhashed"
    db_account = account_model.Account(email=account.email, hashed_password=fake_hashed_password,
                                       phone=account.phone)
    db.add(db_account)
    db.commit()
    db.refresh(db_account)
    return db_account


@log_error
def get_groups(db: Session, skip: int = 0, limit: int = 100):
    return db.query(account_model.Group).offset(skip).limit(limit).all()


@log_error
def get_channels(db: Session, skip: int = 0, limit: int = 100):
    return db.query(account_model.Channel).offset(skip).limit(limit).all()


@log_error
def create_account_group(db: Session, group: schema_account.GroupCreate, account_uid: str):
    db_group = account_model.Group(**group.dict(), owner_id=uuid.UUID(account_uid).hex)
    db.add(db_group)
    db.commit()
    db.refresh(db_group)
    return db_group


@log_error
def create_account_channel(db: Session, channel: schema_account.ChannelCreate, account_uid: str):
    db_channel = account_model.Channel(**channel.dict(), owner_id=uuid.UUID(account_uid).hex)
    db.add(db_channel)
    db.commit()
    db.refresh(db_channel)
    return db_channel


def get_account_by_phone(db: Session, phone: str):
    return db.query(account_model.Account).filter(account_model.Account.phone == phone).first()
