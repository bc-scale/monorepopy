from sqlalchemy import Boolean, Column, ForeignKey, Integer, String
from sqlalchemy.orm import relationship
from sqlalchemy.dialects.postgresql import UUID
import uuid
from core.database import Base


class Account(Base):
    __tablename__ = "accounts"

    uid = Column(UUID(as_uuid=True), primary_key=True, default=uuid.uuid4, index=True)
    email = Column(String, unique=True, index=True)
    phone = Column(String, unique=True, index=True)
    hashed_password = Column(String)
    is_active = Column(Boolean, default=True)

    groups = relationship("Group", back_populates="owner")
    channels = relationship("Channel", back_populates="owner")


class Group(Base):
    __tablename__ = "groups"

    uid = Column(UUID(as_uuid=True), primary_key=True, default=uuid.uuid4, index=True)
    title = Column(String, index=True)
    description = Column(String, index=True)
    owner_id = Column(UUID(as_uuid=True), ForeignKey("accounts.uid"))
    domain = Column(String, index=True, default='public')  # private
    owner = relationship("Account", back_populates="groups")


class Channel(Base):
    __tablename__ = "channels"

    uid = Column(UUID(as_uuid=True), primary_key=True, default=uuid.uuid4, index=True)
    title = Column(String, index=True)
    description = Column(String, index=True)
    owner_id = Column(UUID(as_uuid=True), ForeignKey("accounts.uid"))
    domain = Column(String, index=True, default='public')  # private

    owner = relationship("Account", back_populates="channels")
