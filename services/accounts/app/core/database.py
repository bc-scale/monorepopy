from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from .config import POSTGRES_HOST, POSTGRES_PORT, POSTGRES_DB_NAME, POSTGRES_USER, POSTGRES_PASSWORD, SSL_MODE


SQLALCHEMY_DATABASE_URL = f"postgresql://{POSTGRES_USER}:{POSTGRES_PASSWORD}@{POSTGRES_HOST}:{POSTGRES_PORT}/{POSTGRES_DB_NAME}"

engine = create_engine(
    SQLALCHEMY_DATABASE_URL,
    connect_args={'sslmode': SSL_MODE, 'target_session_attrs': 'read-write'}
)
SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)

Base = declarative_base()

#Base.metadata.create_all(engine)
# Base.metadata.drop_all(engine)
# Base.metadata.create_all(engine)
