import uvicorn
import requests
import re

from fastapi import Depends, FastAPI, HTTPException
from fastapi.responses import JSONResponse
from sqlalchemy.orm import Session
from starlette_exporter import PrometheusMiddleware, handle_metrics

from core import crud
from core.database import SessionLocal, engine
from core.models import account
from core.schemas import schema_account
from core.logger_elk import logger_elk
from core.monitoring_metrics import METRIC_ACCOUNTS_REQUESTS, METRIC_ACCOUNTS_EXCEPTIONS, TIME

account.Base.metadata.drop_all(bind=engine)
account.Base.metadata.create_all(bind=engine)

app = FastAPI(title='Accounts')

app.add_middleware(PrometheusMiddleware)
app.add_route("/metrics", handle_metrics)


# Dependency
def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()


@app.post("/accounts/", response_model=schema_account.Account, tags=["Account"])
@TIME
async def create_account(account: schema_account.AccountCreate, db: Session = Depends(get_db)):
    regex_email = r'\b[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Z|a-z]{2,}\b'
    regex_phone = r'[87]\d{,11}$'
    if not re.fullmatch(regex_email, account.email):
        raise HTTPException(status_code=400, detail='Email is not valid')

    if not re.fullmatch(regex_phone, account.phone):
        raise HTTPException(status_code=400, detail='Phone is not valid')

    db_account = crud.get_account_by_email(db, email=account.email)
    if db_account:
        logger_elk.error(f'Email for {account} already registered')
        METRIC_ACCOUNTS_EXCEPTIONS.labels(code='400', detail='Email already registered').inc()
        raise HTTPException(status_code=400, detail="Email already registered")
    db_account = crud.get_account_by_phone(db, phone=account.phone)
    if db_account:
        METRIC_ACCOUNTS_EXCEPTIONS.labels(code='400', detail='Phone already registered').inc()
        logger_elk.error(f'Phone for {account} already registered')
        raise HTTPException(status_code=400, detail="Phone already registered")

    # Микросервис авторизации
    request_data = {
        'phone': account.phone,
        'password': account.password
    }
    try:
        requests.post('http://authorization:8081/signup', json=request_data)
    except:
        logger_elk.error(f'Bad request for http://authorization:8081/signup with {request_data}')
        raise HTTPException(status_code=400, detail='Bad request')

    METRIC_ACCOUNTS_REQUESTS.labels(method='post', endpoint='/accounts').inc()
    return crud.create_account(db=db, account=account)


@app.get("/accounts/", response_model=list[schema_account.Account], tags=["Account"])
@TIME
async def read_accounts(skip: int = 0, limit: int = 100, db: Session = Depends(get_db)):
    accounts = crud.get_accounts(db, skip=skip, limit=limit)
    METRIC_ACCOUNTS_REQUESTS.labels(method='get', endpoint='/accounts').inc()
    return accounts


@app.get("/accounts/{account_uid}", response_model=schema_account.Account, tags=["Account"])
@TIME
async def read_account(account_uid: str, db: Session = Depends(get_db)):
    db_account = crud.get_account(db, account_uid=account_uid)
    if db_account is None:
        logger_elk.error(f'Account {account_uid} not found')
        METRIC_ACCOUNTS_EXCEPTIONS.labels(code='404', detail='Account not found').inc()
        raise HTTPException(status_code=404, detail="Account not found")

    METRIC_ACCOUNTS_REQUESTS.labels(method='get', endpoint='/accounts/{account_uid}').inc()
    return db_account


@app.post("/accounts/{account_uid}/groups/", response_model=schema_account.Group, tags=["Groups"])
@TIME
async def create_group_for_account(
    account_uid: str, group: schema_account.GroupCreate, db: Session = Depends(get_db)
):
    METRIC_ACCOUNTS_REQUESTS.labels(method='post', endpoint='/accounts/{account_uid}/groups').inc()
    return crud.create_account_group(db=db, group=group, account_uid=account_uid)


@app.post("/accounts/{account_uid}/channels/", response_model=schema_account.Channel, tags=["Channels"])
@TIME
async def create_group_for_account(
    account_uid: str, channel: schema_account.ChannelCreate, db: Session = Depends(get_db)
):
    METRIC_ACCOUNTS_REQUESTS.labels(method='post', endpoint='/accounts/{account_uid}/channels').inc()
    return crud.create_account_channel(db=db, channel=channel, account_uid=account_uid)


@app.get("/groups/", response_model=list[schema_account.Group], tags=["Groups"])
@TIME
async def read_groups(skip: int = 0, limit: int = 100, db: Session = Depends(get_db)):
    groups = crud.get_groups(db, skip=skip, limit=limit)
    METRIC_ACCOUNTS_REQUESTS.labels(method='get', endpoint='/groups').inc()
    return groups


@app.get("/channels/", response_model=list[schema_account.Channel], tags=["Channels"])
@TIME
async def read_groups(skip: int = 0, limit: int = 100, db: Session = Depends(get_db)):
    groups = crud.get_channels(db, skip=skip, limit=limit)
    METRIC_ACCOUNTS_REQUESTS.labels(method='get', endpoint='/channels').inc()
    return groups


@app.post("/accounts/login", response_model=schema_account.Token, tags=["Account"],
          status_code=200, response_class=JSONResponse)
def login_account(account: schema_account.AuthAccount, db: Session = Depends(get_db)):
    # Микросервис авторизации
    request_data = {
        'phone': account.phone,
        'password': account.password
    }
    try:
        response = requests.post('http://authorization:8081/login', json=request_data)
    except Exception:
        logger_elk.error(f'Bad request for http://authorization:8081/login with {request_data}')
        raise HTTPException(status_code=400, detail='Bad request')

    if response.status_code == 200:
        METRIC_ACCOUNTS_REQUESTS.labels(method='post', endpoint='/accounts/login').inc()
        return response.json()
    else:
        logger_elk.error(f'POST /accounts/login error: User or password incorrect')
        raise HTTPException(status_code=400, detail='User or password incorrect')


if __name__ == '__main__':
    logger_elk.info('Start application accounts')
    uvicorn.run(app, host="0.0.0.0", port=8001)
