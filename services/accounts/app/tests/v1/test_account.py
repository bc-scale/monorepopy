import sys
import uuid
from os.path import dirname
sys.path.append(dirname(dirname(dirname(__file__))))
from tests.client import client


def test_create_account():
    response = client.post(
        "/accounts/",
        json={"email": "deadpool@example.com", "password": "chimichangas4life", "phone": "79161111111"},
    )
    assert response.status_code == 200, response.text
    data = response.json()
    assert data["email"] == "deadpool@example.com"
    assert "uid" in data
    account_uid = data["uid"]

    response = client.get(f"/accounts/{account_uid}")
    assert response.status_code == 200, response.text
    data = response.json()
    assert data["email"] == "deadpool@example.com"
    assert data["uid"] == account_uid

    bad_account_uid = uuid.uuid4()
    response = client.get(f"/accounts/{bad_account_uid}")
    assert response.status_code == 404, response.text
    assert response.json() == {"detail":"Account not found"}

    # Registered email account
    response = client.post(
        "/accounts/",
        json={"email": "deadpool@example.com", "password": "chimichangas4life", "phone": "79161111111"},
    )
    assert response.status_code == 400, response.text
    assert response.json() == {"detail": "Email already registered"}

    # Registered phone
    response = client.post(
        "/accounts/",
        json={"email": "big_deadpool@example.com", "password": "chimichangas4life", "phone": "79161111111"},
    )
    assert response.status_code == 400, response.text
    assert response.json() == {"detail": "Phone already registered"}


def test_list_accounts():
    response = client.get('/accounts/')
    assert response.status_code == 200, response.text
    assert isinstance(response.text, str)


def test_read_account():
    response = client.get('/accounts/')
    data = response.text
    uid = data.split(',')[2].split('"')[3]
    response = client.get(f'/accounts/{uid}')
    assert response.status_code == 200


def test_create_group_for_account():
    response = client.get('/accounts/')
    data = response.text
    account_uid = data.split(',')[2].split('"')[3]
    response = client.post(f'/accounts/{account_uid}/groups/',
                           json={"title": "string", "description": "string"})

    assert response.status_code == 200, response.text


def test_create_channels_for_account():
    response = client.get('/accounts/')
    data = response.text
    account_uid = data.split(',')[2].split('"')[3]
    response = client.post(f'/accounts/{account_uid}/channels/',
                           json={"title": "string", "description": "string"})
    assert response.status_code == 200, response.text


def test_list_groups():
    response = client.get('/groups/')
    assert response.status_code == 200, response.text
    assert isinstance(response.text, str)


def test_list_channels():
    response = client.get('/channels/')
    assert response.status_code == 200, response.text
    assert isinstance(response.text, str)


def test_login_account():
    data = {"password": "chimichangas4life", "phone": "79161111111"}
    response = client.post('/accounts/login', json=data)
    assert response.status_code == 200, response.text


def test_bad_login_account():
    data = {"password": "chimichangas4life", "phone": "79161111111"}
    response = client.post('/accounts/login', data=data)
    assert response.status_code == 422, response.text
