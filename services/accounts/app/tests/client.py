from starlette.testclient import TestClient
from tests.database import TestingSessionLocal, clear_db
from main import app, get_db


def override_get_db():
    try:
        db = TestingSessionLocal()
        yield db
    finally:
        db.close()


# Clear Database
clear_db(db_session=TestingSessionLocal())

app.dependency_overrides[get_db] = override_get_db

client = TestClient(app)
