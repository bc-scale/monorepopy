from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from core.database import Base
from core.config import POSTGRES_HOST, POSTGRES_PORT, POSTGRES_DB_NAME, POSTGRES_USER, POSTGRES_PASSWORD

from core.models.account import Account, Group, Channel

SQLALCHEMY_DATABASE_URL = f"postgresql://{POSTGRES_USER}:{POSTGRES_PASSWORD}@{POSTGRES_HOST}:{POSTGRES_PORT}/{POSTGRES_DB_NAME}"

engine = create_engine(
    SQLALCHEMY_DATABASE_URL
)

TestingSessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)


Base.metadata.create_all(bind=engine)


def override_get_db() -> TestingSessionLocal:
    try:
        db = TestingSessionLocal()
        yield db
    finally:
        db.close()


def clear_db(db_session: TestingSessionLocal) -> None:
    try:
        db_session.query(Channel).delete()
        db_session.query(Group).delete()
        db_session.query(Account).delete()
        db_session.commit()
    except Exception as er:
        db_session.rollback()
        raise er
